# Python C1: Agendador da "Professora Portugal"

## O que vamos fazer

- criar uma agenda que possibilite:
  saber quais horários estão disponíveis para certo dia;
  quais alunos estão agendados para certo dia;
  acessar a descrição para cada aula;
  saber exatamente o que foi visto na última aula;
  organizar as anotações para as próximas aulas.

- realizar http requests

- read/write em arquivos csv

## Considerações

```shell
  import requests
```

- criar o arq .env

```shell
  FLASK_APP=app.py
  FLASK_ENV=development
```

- criar o arq .env.example e retirar var de amb sensíveis

```shell
  FLASK_APP=app.py
  FLASK_ENV=development
```

- versionar com git

- incluir no .gitignore

```shell
  venv
  .env
  **/__pycache__
  **.swp
  .vscode
```

- comandos

```shell
  python -m venv venv
  source venv/bin/activate
  pip install pytest
  pip install environs
  python -m pip install -U pip
  pip install flask
  python -m pip install requests
  pip freeze > requirements.txt
  flask run
```

## funções úteis

- verificar arquivo inexistente ou vazio

```shell
  import os.path

  file_exists = os.path.isfile(filename)

  if not file_exists:
    print('arquivo não existe')

  if os.stat(filename).st_size == 0:
    print('arquivo vazio)
```

- rodar o mesmo teste com diferentes parâmetros

```shell
@pytest.mark.parametrize('input, output', [
    ([9, 15, 16, 17, 18, 20, 21, 22], ['9:00', '15:00-18:00', '20:00-22:00']),
    ([15, 16, 17, 18, 20, 21, 22], ['15:00-18:00', '20:00-22:00']),
    ([9, 23], ['9:00', '23:00']),
    ([15, 16, 18, 20, 21, 23], ['15:00-16:00', '18:00', '20:00-21:00', '23:00'])
])
def test_stringify_grouped_list(input, output):
    grouped_hours_list = sort_and_group(input)
    result = stringify_grouped_list(grouped_hours_list)

    expected = output

    assert result == expected
```
