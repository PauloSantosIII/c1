from flask import Flask
from app.views.index_view import bp as index_bp
from app.views.appointment_view import bp as appointment_bp
from app.views.home_view import bp as home_bp


def create_app():
    app = Flask(__name__)

    app.register_blueprint(index_bp)
    app.register_blueprint(home_bp)
    app.register_blueprint(appointment_bp)

    return app