from flask import Blueprint, render_template, url_for

bp = Blueprint('index_route', __name__)


@bp.route('/')
def index():
    css_url = url_for('static', filename='index_styles.css')
    return render_template('index_template.html', css_url=css_url)
