from flask import Blueprint, render_template, url_for

bp = Blueprint('home_route', __name__)


@bp.route('/home')
def home():
    css_url = url_for('static', filename='home_styles.css')
    return render_template('home_template.html', css_url=css_url)
