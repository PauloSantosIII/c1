from flask import Blueprint, request
from environs import Env
import csv
from app.services.appointments.appointment_csv_services import save_appointment, get_next_appointment_id, set_up_date, get_appointment_hour, get_formatted_date, sort_and_group, stringify_grouped_list, format_input_date, get_available_times, set_appointment_date, delete_appointment

env = Env()
env.read_env()

bp = Blueprint('appointment_route', __name__)


@bp.route('/appointment', methods=['POST'])
def make_appointment():

    # res = request.form
    # if res['name'] != '':
    #     print('from form')

    # appointment = {
    #     "id": 1,
    #     "date": request.form["date"],
    #     "name": request.form["name"],
    #     "school-subjects": request.form["school-subjects"],
    #     "difficulty": request.form["difficulty"],
    #     "class-number": request.form["class-number"],
    #     "_growth": request.form["_growth"]
    # }

    # return appointment

    filename = env('APPOINTMENTS_CSV')
    appointment = request.get_json()

    # Agenda somente entre 8 e 23h
    hour = int(get_appointment_hour(appointment))
    if not 8 <= hour <= 23:
        return 'Horário indisponível'

    # Arredonda os horários
    appointment = set_up_date(appointment)

    # Salva os appointments
    save_appointment(appointment, filename)

    # Retorna os dados do agendamento realizado
    id = get_next_appointment_id(filename) - 1

    res = {
        "id": id,
        "date": appointment["date"],
        "name": appointment["name"],
        "school-subjects": appointment["school-subjects"],
        "difficulty": appointment["difficulty"],
        "class-number": appointment["class-number"],
        "_growth": appointment["_growth"]
    }

    return res


@bp.route('/appointment', methods=['GET'])
def list_appointment():
    filename = env('APPOINTMENTS_CSV')
    res = []

    with open(filename, 'r') as f:
        reader = csv.DictReader(f)
        for index, line in enumerate(reader):
            res.append(line)

    return {'data': res}


@bp.route('/appointment/available_times_on_the_day', methods=['GET'])
def available_times():
    date = request.args.get('date')
    filename = env('APPOINTMENTS_CSV')

    available_times = get_available_times(filename, date)
    available_times = sort_and_group(available_times)
    available_times = stringify_grouped_list(available_times)

    return {'available-times': available_times}


@bp.route('/appointment/<id>', methods=['PATCH'])
def appointment(id):
    filename = env('APPOINTMENTS_CSV')
    new_info = request.get_json()
    updated_appointment = set_appointment_date(filename, id, new_info)

    return updated_appointment


@bp.route('/appointment/<id>', methods=['DELETE'])
def del_appointment(id):
    filename = env('APPOINTMENTS_CSV')
    delete_appointment(filename, id)
    return 'ok'
