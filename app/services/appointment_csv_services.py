import csv
import os.path
from datetime import date, datetime
import pytz
from environs import Env

env = Env()
env.read_env()


def get_next_appointment_id(filename):

    last_id = 0

    with open(filename, 'r') as file:

        if os.stat(filename).st_size == 0:
            return 1

        reader = csv.DictReader(file)
        for row in reader:
            last_id = int(row.get('id', 0))

    return last_id + 1


def get_appointment_hour(appointment):
    appointment_date_list = appointment["date"].split(" ")

    hour = appointment_date_list[4].split(":")

    return hour[0]


def set_up_date(appointment):
    appointment_date_list = appointment["date"].split(" ")

    hour = appointment_date_list[4].split(":")
    hour = hour[0] + ":00:00"
    appointment_date_list[4] = hour

    appointment_date = " ".join(appointment_date_list)
    appointment["date"] = appointment_date

    return appointment


def save_appointment(appointment, filename):
    file_exists = os.path.isfile(filename)

    if not file_exists:
        next_id = 1
    else:
        next_id = get_next_appointment_id(filename)

    with open(filename, 'a+', newline='') as f:
        fieldnames = ['id', 'date', 'name', 'school-subjects',
                      'difficulty', 'class-number', '_growth']

        writer = csv.DictWriter(f, fieldnames=fieldnames)

        if not file_exists:
            writer.writeheader()

        if os.stat(filename).st_size == 0:
            writer.writeheader()

        writer.writerow({
            'id': next_id,
            'date': appointment['date'],
            'name': appointment['name'],
            'school-subjects': appointment['school-subjects'],
            'difficulty': appointment['difficulty'],
            'class-number': appointment['class-number'],
            '_growth': appointment['_growth']
        })

    return 'ok'


def file_reader(filename):
    content = []

    with open(filename, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            content.append(row)

    return content


def file_rewrite(filename, content):
    with open(filename, 'w') as f:
        writer = csv.writer(f)
        for row in content:
            writer.writerow(row)


def set_up_original_content(filename):
    content = file_reader(filename)

    with open(filename, 'w') as f:
        writer = csv.writer(f)
        for row in content[:-1]:
            writer.writerow(row)

    return 'ok'


# não funciona corretamente a timezone
def get_formatted_date(unformatted_date):
    searched_date = datetime(2018, 6, 1, tzinfo=pytz.utc)
    print(searched_date.strftime("%a %b %d %Y %H:%M:%S GMT%z (%Z)"))


# transforma "Tue Sep 08 2020 14:22:13 GMT-0300" em object datetime
def format_date_from_db(date):
    formatted_date = datetime.strptime(
        date, '%a %b %d %Y %H:%M:%S %Z%z')
    return formatted_date


# transforma "02012021" em object datetime
def format_input_date(date):
    formatted_date = datetime.strptime(date, '%d%m%Y')
    return formatted_date


def sort_and_group(hours_list):

    # configuração da lista - int e ordenada
    hours_list = [int(hour) for hour in hours_list]
    hours_list = sorted(hours_list)

    result = []

    # var para armazenar index dos números consecutivos, que são armazenados em result no mesmo loop
    already_read = 0

    for index in range(len(hours_list)):

        # armazena tenporariamente um número da lista, se o próximo não for consecutivo, ele será adicionado à lista result
        temp = [hours_list[index]]

        # se o número referente ao index já foi lido, pula o loop
        if index <= already_read and index != 0:
            continue

        # se o próximo index ainda for menor que o tamanho da lista, enquanto o próximo num for consecutivo, guarda na var temporária
        if index+1 < len(hours_list):
            while hours_list[index] + 1 == hours_list[index+1]:

                temp.append(hours_list[index+1])

                index += 1
                already_read = index

                if index+1 >= len(hours_list):
                    break
                num = hours_list[index]

        result.append(temp)

    return result


def stringify_grouped_list(grouped_hours_list):
    result = []
    temp = []

    for index, group in enumerate(grouped_hours_list):
        for i, num in enumerate(group):

            # somente o primeiro e o último num são transf em string
            if i == 0 or i == len(group) - 1:
                temp.append(f'{num}:00')

        result.append('-'.join(temp))
        temp = []

    return result


def get_available_times(filename, input_date):

    # lista das possíveis horas de trabalho
    work_beginning = 8
    work_ending = 24
    available_times = [hour for hour in range(work_beginning, work_ending)]

    # transforma a string input_date "02012021" em object datetime
    input_date = format_input_date(input_date)

    # cria uma lista com os horários indisponíveis relacionados à input_date
    unavailable_times = []
    with open(filename, 'r') as f:
        reader = csv.DictReader(f)
        for appointment in reader:
            appointment_date = format_date_from_db(appointment['date'][:-6])
            if appointment_date.date() == input_date.date():
                unavailable_times.append(appointment_date.hour)

    # remove horas duplicadas
    unavailable_times = list(dict.fromkeys(unavailable_times))

    # atualiza a lista de horários disponíveis
    for hour in unavailable_times:
        available_times.remove(hour)

    return available_times


def set_appointment_date(filename, id, new_info):
    appointments_data = []
    result = {}

    with open(filename, 'r') as f:
        reader = csv.DictReader(f)

        for appointment in reader:
            if appointment['id'] == str(id):
                appointment['date'] = new_info['date']
                appointment = set_up_date(appointment)
                result = appointment
            appointments_data.append(appointment)

    with open(filename, 'w') as f:
        fieldnames = ['id', 'date', 'name', 'school-subjects',
                      'difficulty', 'class-number', '_growth']
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(appointments_data)

    return result


def delete_appointment(filename, id):
    appointments_data = []
    result = {}

    with open(filename, 'r') as f:
        reader = csv.DictReader(f)

        for appointment in reader:
            if appointment['id'] == str(id):
                continue
            appointments_data.append(appointment)

    with open(filename, 'w') as f:
        fieldnames = ['id', 'date', 'name', 'school-subjects',
                      'difficulty', 'class-number', '_growth']
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(appointments_data)
