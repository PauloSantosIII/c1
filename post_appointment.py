import requests

body = {
    "date": "Tue Sep 08 2020 15:52:13 GMT-0300 (-03)",
    "name": "Son Goku",
    "school-subjects": "Matemática",
    "difficulty": "Não consegue entender como escalonar matrizes",
    "class-number": 3,
    "_growth": "Agora o aluno já entendeu que sistemas de equações podem ser representado por matrizes"
}

res = requests.post('http://localhost:5000/appointment', json=body)
if res.ok:
    appointment_data = res.json()
    print(appointment_data)
